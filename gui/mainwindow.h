#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsItem>

namespace Ui {
class MainWindow;
}

class Swarm;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void loadButtonPressed();
    void swarmButtonPressed();
    void sobelButtonPressed();
    void cannyButtonPressed();

    void edgeDetectionFinished(QImage image);
    void swarmUpdate(QImage image);
    void swarmFinished(QImage image);

private:
    Ui::MainWindow *ui;

    Swarm *m_swarm;

    QImage m_workImage;
    QImage m_displayImage;

    void showImage(QImage image);
    void placeAndCenter(QGraphicsPixmapItem *item, QGraphicsScene* scene);
};

#endif // MAINWINDOW_H
