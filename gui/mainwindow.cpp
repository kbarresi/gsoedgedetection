#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "../swarm/swarm.h"

#include "../edge_detectors/sobeledgefilter.h"
#include "../edge_detectors/cannyedgefilter.h"

#include "../util/util.h"

#include <QFileDialog>
#include <QDebug>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->graphicsView->setScene(new QGraphicsScene(ui->graphicsView));

    ui->loadImageButton->setEnabled(true);
    ui->cannyButton->setDisabled(true);
    ui->swarmButton->setDisabled(true);
    ui->sobelButton->setDisabled(true);


    connect(ui->loadImageButton, SIGNAL(pressed()), SLOT(loadButtonPressed()));
    connect(ui->cannyButton, SIGNAL(pressed()), SLOT(cannyButtonPressed()));
    connect(ui->swarmButton, SIGNAL(pressed()), SLOT(swarmButtonPressed()));
    connect(ui->sobelButton, SIGNAL(pressed()), SLOT(sobelButtonPressed()));

    m_swarm = 0;
}

MainWindow::~MainWindow()
{
    delete ui;

    if (m_swarm)
        delete m_swarm;
}

void MainWindow::loadButtonPressed() {
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select Image File"),
                                                    QDir::currentPath() + "/../GSOEdgeDetection/test images",
                                                    tr("Image Files(*.png *.bmp *.jpg *.jpeg)"));
    QFile file(fileName);
    if (!file.exists()) {
        qDebug() << "No file given";
        file.close();
        return;
    }

    file.close();

    qDebug() << "Opening: " << fileName;

    QImage image(fileName);
    image = image.convertToFormat(QImage::Format_RGB32);

    if (image.isNull()) {
        qDebug() << "\t***Error*** Couldn't load image file";
        return;
    } else
        qDebug() << "\t...success";

    m_workImage = image;    //keep a copy of the original image size for working on...
    showImage(image);

    ui->swarmButton->setEnabled(true);
    ui->cannyButton->setEnabled(true);
    ui->sobelButton->setEnabled(true);
}

void MainWindow::swarmButtonPressed() {
    if (m_swarm) {
        delete m_swarm;
        m_swarm = 0;
    }


    m_swarm = new Swarm(importImage(m_workImage), m_workImage.width(), m_workImage.height(), 0);
    connect(m_swarm, SIGNAL(finished(QImage)), SLOT(swarmFinished(QImage)));
    connect(m_swarm, SIGNAL(update(QImage)), SLOT(swarmUpdate(QImage)));

    QThread *swarmThread = new QThread(this);
    connect(swarmThread, SIGNAL(started()), m_swarm, SLOT(run()));

    m_swarm->moveToThread(swarmThread);
    swarmThread->start();
}

void MainWindow::cannyButtonPressed() {
    ImageMatrix image = importImage(m_workImage);
    CannyEdgeFilter *filter = new CannyEdgeFilter(image, m_workImage.width(), m_workImage.height());
    filter->run();

    ImageMatrix result = filter->getImage();
    QImage display = exportImage(result, m_workImage.width(), m_workImage.height());


    m_workImage = display;    //keep a copy of the original image size for working on...
    showImage(display);

    delete filter;
    free(result);
    free(image);
}

void MainWindow::sobelButtonPressed() {
    ImageMatrix image = importImage(m_workImage);

    SobelEdgeFilter *filter = new SobelEdgeFilter(image, m_workImage.width(), m_workImage.height());
    filter->run();

    ImageMatrix result = filter->getImage();

    QImage display = exportImage(result, m_workImage.width(), m_workImage.height());

    m_workImage = display;    //keep a copy of the original image size for working on...
    showImage(display);

    free(result);
    free(image);
    delete filter;
}


void MainWindow::edgeDetectionFinished(QImage image) {
    Q_UNUSED(image);
}

void MainWindow::swarmUpdate(QImage image) {
    showImage(image);
}
void MainWindow::swarmFinished(QImage image) {
    showImage(image);
}





void MainWindow::placeAndCenter(QGraphicsPixmapItem *item, QGraphicsScene* scene) {
    scene->addItem(item);

    QPointF sceneOrigin = scene->views().first()->mapToScene(QPoint(0, 0));

    item->setPos(sceneOrigin.x(), sceneOrigin.y());
}
void MainWindow::showImage(QImage image) {
    m_displayImage = image.scaled(ui->graphicsView->width(),    //and a shrunk size for display
                                ui->graphicsView->height(),
                                Qt::KeepAspectRatio, Qt::SmoothTransformation);


    ui->graphicsView->scene()->clear();

    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QPixmap::fromImage(m_displayImage));
    placeAndCenter(item, ui->graphicsView->scene());

    qDebug() << "Updated image";
}
