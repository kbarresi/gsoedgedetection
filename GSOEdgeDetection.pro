#-------------------------------------------------
#
# Project created by QtCreator 2014-01-29T11:54:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GSOEdgeDetection
TEMPLATE = app


SOURCES += main.cpp\
        gui/mainwindow.cpp \
    swarm/glowworm.cpp \
    swarm/swarm.cpp \
    edge_detectors/sobeledgefilter.cpp \
    edge_detectors/cannyedgefilter.cpp

HEADERS  += gui/mainwindow.h \
    swarm/glowworm.h \
    swarm/swarm.h \
    edge_detectors/sobeledgefilter.h \
    edge_detectors/cannyedgefilter.h \
    util/util.h

FORMS    += gui/mainwindow.ui
