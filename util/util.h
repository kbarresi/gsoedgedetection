#ifndef UTIL_H
#define UTIL_H

#include <QImage>

#define PI 3.14159265

const int SWARM_ITERATIONS = 5000;
const int SWARM_SIZE = 2500;

const int WORM_VISION = 50;
const int WORM_RADIAL_RANGE = 100;
const int WORM_STEP_SIZE = 25;
const int WORM_THRESH = 5;
const int WORM_BETA = 20;
const double WORM_GAMMA = 0.75;
const double WORM_DECAY = 0.5;

const int CANNY_THRESH = 10;
const int SOBEL_THRESH = 100;


typedef int** ImageMatrix;
typedef int SobelKernel[3][3];
typedef int GaussianFilter[5][5];


const SobelKernel SobelKernelX = {{ 1, 0, -1},
                                  { 2, 0, -2},
                                  { 1, 0, -1}};
const SobelKernel SobelKernelY = {{ 1, 2, 1},
                                  { 0, 0, 0},
                                  {-1, -2, -1}};
const GaussianFilter GuassianSmoother = {{2, 4, 5, 4, 2},
                                        { 4, 9, 12, 9, 4},
                                        { 5, 12, 15, 12, 5},
                                        { 4, 9, 12, 9, 4},
                                        { 2, 4, 5, 4 , 2} };



















static ImageMatrix importImage(QImage image) {
    ImageMatrix output = (ImageMatrix) malloc(sizeof(int*) * image.width());
    for (int i = 0; i < image.width(); i++)
        output[i] = (int*) malloc(sizeof(int*) * image.height());

    QRgb* colorPtr = 0;
    for (int y = 0; y < image.height(); y++) {
        colorPtr = (QRgb*) image.scanLine(y);
        for (int x = 0; x < image.width(); x++) {
            int grayscale = (qRed(*colorPtr) + qGreen(*colorPtr) + qBlue(*colorPtr)) / 3;
            output[x][y] = grayscale;
            colorPtr++;
        }
    }
    return output;
}

static QImage exportImage(ImageMatrix image, int width, int height, bool showColor = false) {
    QImage output = QImage(QSize(width, height), QImage::Format_RGB32);
    QRgb *colorPtr = 0;
    for (int y = 0; y < height; y++) {
        colorPtr = (QRgb*) output.scanLine(y);
        for (int x = 0; x < width; x++) {
            QRgb color;
            if (showColor && image[x][y] == 256)
                color = qRgb(0, 255, 128);
            else
                color = qRgb(image[x][y], image[x][y], image[x][y]);
            *colorPtr = color;
            colorPtr++;
        }
    }
    return output;
}

#endif // UTIL_H
