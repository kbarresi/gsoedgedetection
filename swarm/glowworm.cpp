#include "glowworm.h"
#include "../util/util.h"
#include <qmath.h>

GlowWorm::GlowWorm()
{
    m_x = m_y = 0;
    m_intensity = 0;
    m_localDecisionRange = WORM_RADIAL_RANGE;
}

qreal GlowWorm::distance(GlowWorm *worm) {
    qreal xMag = qPow(worm->x() - m_x, 2);
    qreal yMag = qPow(worm->y() - m_y, 2);
    return qSqrt(xMag + yMag);
}
