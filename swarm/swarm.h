#ifndef SWARM_H
#define SWARM_H

#include <QObject>
#include "../util/util.h"

class GlowWorm;
class Swarm : public QObject
{
    Q_OBJECT
public:

    Swarm(ImageMatrix image, int width, int height, QObject *parent = 0);


public slots:
    void run();

signals:
    void update(QImage image);
    void finished(QImage image);



private:

    QList<GlowWorm*> m_worms;

    ImageMatrix m_image;
    ImageMatrix m_workImage;

    int m_width;
    int m_height;


    void updateLuminescence();
    void movement();

    void prepareUpdate();
    void findNeighbors(QList<GlowWorm*>* neighbors, GlowWorm* worm);
    void moveRandomly(GlowWorm* worm);

    void updateDecisionRange(GlowWorm* worm);
    qreal positionFitness(int x, int y);
};

#endif // SWARM_H
