#ifndef GLOWWORM_H
#define GLOWWORM_H

#include <QObject>

class GlowWorm
{
public:
    GlowWorm();

    int x() { return m_x; }
    int y() { return m_y; }
    void setX(int pos) { m_x = pos; }
    void setY(int pos) { m_y = pos; }
    void setPos(int xPos, int yPos) { m_x = xPos; m_y = yPos; }

    int localDecisionRange() { return m_localDecisionRange; }
    void setLocalDecisionRange(int r) { m_localDecisionRange = r; }

    qreal intensity() { return m_intensity; }
    void setIntensity(qreal i) { m_intensity = i; }


    qreal distance(GlowWorm* worm);
private:
    int m_x;
    int m_y;

    qreal m_intensity;

    int m_localDecisionRange;

};

#endif // GLOWWORM_H
