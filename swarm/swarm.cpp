#include "swarm.h"
#include "glowworm.h"

#include <QDebug>
#include <qmath.h>

Swarm::Swarm(ImageMatrix image, int width, int height, QObject *parent)
    : QObject(parent)
{
    m_width = width;
    m_height = height;
    m_image = image;

    qsrand(qrand());
}

void Swarm::run() {
    //Create the swarm and scatter them randomly around the image space

    for (int i = 0; i < SWARM_SIZE; i++) {
       int x = qrand() % m_width;
       int y = qrand() % m_height;

        GlowWorm *worm = new GlowWorm();
        if (x < 0 || x > m_width || y < 0 || y > m_height)
            qDebug() << "Bad position";
        worm->setX(x);
        worm->setY(y);

        m_worms.append(worm);
    }

    m_workImage = (ImageMatrix) malloc(sizeof(int*) * m_width);
    for (int i = 0; i < m_width; i++)
        m_workImage[i] = (int*) malloc(sizeof(int*) * m_height);

    prepareUpdate();


    for (int i = 0; i < SWARM_ITERATIONS; i++) {
        qDebug() << "Iteration: " << i;


        updateLuminescence();
        movement();

        if (i % 10 == 0 && i != 0) {
            prepareUpdate();
        }
    }
}


void Swarm::updateLuminescence() {
    for (int i = 0; i < m_worms.size(); i++) {
        GlowWorm* worm = m_worms.at(i);

        int x = worm->x();
        int y = worm->y();

        /* Equation:
         * theta(t+1) = max{0, (1 - p) * theta(t)  + gammaJ(t + 1)}
         *      ro(t + 1) = new brightness level
         *      ro(t) = previous brightness level
         *      p = decay constant [0, 1]
         *      gamma = proportionality constant
         *      J(t + 1) = fitness of current position
         */
        qreal oldIntensity = worm->intensity();
        qreal fitness = positionFitness(x, y);

        qreal newIntensity = qMax((qreal)0, (((qreal)1 - WORM_DECAY) * oldIntensity) + (WORM_GAMMA * fitness));
        worm->setIntensity(newIntensity);
    }
}
void Swarm::movement() {
    for (int i = 0; i < m_worms.size(); i++) {
        GlowWorm* worm = m_worms.at(i);
        GlowWorm* bestWorm = 0;
        for (int j = 0; j < m_worms.size(); j++) {
            if (i == j)
                continue;
            GlowWorm* wormTwo = m_worms.at(j);

            qreal distance = qSqrt(qPow(wormTwo->x() - worm->x(), 2) + qPow(wormTwo->y() - worm->y(), 2));
            if (distance > worm->localDecisionRange()  || distance < 5)
                continue;

            if (wormTwo->intensity() > worm->intensity() + WORM_THRESH) {
                if (bestWorm == 0)
                    bestWorm = wormTwo;
                else if (bestWorm->intensity() < wormTwo->intensity())
                    bestWorm = wormTwo;
            }
        }

        if (bestWorm != 0) {
            GlowWorm *target = bestWorm;
            int deltaX = target->x() - worm->x();
            int deltaY = target->y() - worm->y();
            qreal wormDistance = qSqrt((deltaX * deltaX) + (deltaY * deltaY));

            int movementDistance= qMin((qrand() % WORM_STEP_SIZE + 1), (int)wormDistance);

            qreal newX = (((qreal)deltaX / wormDistance) * movementDistance) + worm->x();
            qreal newY = (((qreal)deltaY / wormDistance) * movementDistance) + worm->y();

            for (int j = 0; j < m_worms.size(); j++) {  //make sure to give the worm some room, i.e. not on another worm
                if (i == j)
                    continue;
                GlowWorm *checkWorm = m_worms.at(j);
                if (checkWorm->x() == newX && checkWorm->y() == newY) {  //someone's already there, move shorter...
                    movementDistance -= 1;
                    newX = (((qreal)deltaX / wormDistance) * movementDistance) + worm->x();
                    newY = (((qreal)deltaY / wormDistance) * movementDistance) + worm->y();
                    break;
                }
            }

            worm->setPos(newX, newY);
        } else {
            moveRandomly(worm);
        }
        updateDecisionRange(worm);
    }
}


void Swarm::prepareUpdate() {
    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++)
            m_workImage[x][y] = m_image[x][y] / 5;
    }


    for (int i = 0; i < m_worms.size(); i++) {
        GlowWorm *worm = m_worms.at(i);
        int x = worm->x();
        int y = worm->y();
        m_workImage[x][y] = 256;
    }

    qDebug() << "Emitting...";
    emit update(exportImage(m_workImage, m_width, m_height, true));
}

qreal Swarm::positionFitness(int x, int y) {

    /*We check out a block of pixels around the current worm to see if it's sitting on a good spot.
     * We define a "good" spot as on with a higher Sobel operator result
    */

    int sumX = 0;
    int sumY = 0;
    for (int j = -1; j <= 1; j++) {
        for (int k = -1; k <= 1; k++) {
            if (j + x >= m_width || j + x < 0 || y + k >= m_height || y + k < 0)
                return 0;
            sumX += SobelKernelX[j + 1][k + 1] * m_image[x + j][y + k];
            sumY += SobelKernelY[j + 1][k + 1] * m_image[x + j][y + k];
        }
    }
    qreal magnitude =qSqrt((sumX * sumX) + (sumY * sumY));
    qreal goodness = magnitude / (qreal)1000;

    return goodness;
}

void Swarm::findNeighbors(QList<GlowWorm *> *neighbors, GlowWorm *worm) {
    for (int i = 0; i < m_worms.size(); i++) {
        GlowWorm* candidate = m_worms.at(i);
        if (candidate == worm)
            continue;

        qreal distance = worm->distance(candidate);
        if (distance <= WORM_RADIAL_RANGE)
            neighbors->append(candidate);
    }
}

void Swarm::moveRandomly(GlowWorm *worm) {
    int distance = qrand() % ((worm->localDecisionRange() * 10) + 1);
    qreal xComponent = ((qreal)qrand()/(qreal)RAND_MAX) * (qreal)distance;
    if (qrand() % 2 == 0)
        xComponent *= -1;
    qreal yComponent = ((qreal)qrand()/(qreal)RAND_MAX) * (qreal)distance;
    if (qrand() % 2 == 0)
        yComponent *= -1;

    int newX = worm->x() + xComponent;
    int newY = worm->y() + yComponent;

    if (newX < 0)
        newX = 0;
    else if (newX >= m_width)
        newX = m_width - 1;
    if (newY < 0)
        newY = 0;
    else if (newY >= m_height)
        newY = m_height - 1;

    qreal newGoodness = positionFitness(newX, newY);
    if (newGoodness * qreal(1.05) >= worm->intensity()) {
        if (newX < 0 || newX > m_width || newY < 0 || newY > m_height)
            qDebug() << "Bad position";
        worm->setPos(newX, newY);
    }
}

void Swarm::updateDecisionRange(GlowWorm *worm) {
    int neighborCount = 0;
    for (int i = 0; i < m_worms.size(); i++) {
        GlowWorm* wormTwo = m_worms.at(i);
        if (worm == wormTwo)
            continue;
        qreal dist = worm->distance(wormTwo);
        if (dist <= worm->localDecisionRange())  //within the sensor range, count it
            neighborCount++;
    }

    //r_d = r_s / (1 + B*D(t))
        //r_s = radial range of sensor (max)
        //B = const
        //D(t) = concentration factor
    //D(t) = N(t) / pi*r^2
        //N(t) = number of neighbors within range
    qreal Dt = (qreal)neighborCount / ((qreal)PI * (qreal)qPow(WORM_RADIAL_RANGE, 2));
    qreal r_d = (qreal)WORM_RADIAL_RANGE / ((qreal)1 + ((qreal)WORM_BETA * Dt));

    worm->setLocalDecisionRange(r_d);
}
