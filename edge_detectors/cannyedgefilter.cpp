#include "cannyedgefilter.h"
#include <qmath.h>

CannyEdgeFilter::CannyEdgeFilter(ImageMatrix image, int width, int height)
{
    m_image = image;
    m_width = width;
    m_height = height;

    m_smoothedImage = (ImageMatrix) malloc(sizeof(int*) * width);
    m_directionImage = (ImageMatrix) malloc(sizeof(int*) * width);
    m_gradientImage = (ImageMatrix) malloc(sizeof(int*) * width);
    m_finalImage = (ImageMatrix) malloc(sizeof(int*) * width);
    for (int i = 0; i < width; i++) {
        m_smoothedImage[i] = (int*) malloc(sizeof(int*) * height);
        m_directionImage[i] = (int*) malloc(sizeof(int*) * height);
        m_gradientImage[i] = (int*) malloc(sizeof(int*) * height);
        m_finalImage[i] = (int*) calloc(height, sizeof(int*));
    }

}
CannyEdgeFilter::~CannyEdgeFilter() {
    free(m_smoothedImage);
    free(m_directionImage);
    free(m_gradientImage);
}


void CannyEdgeFilter::run() {
    qreal gaussianFactor = (qreal)1 / qreal(159);
    for (int x = 2; x < m_width -2; x++) {
        for (int y = 2; y < m_height - 2; y++) {
            //Smooth with a gaussian filter:
            int sum = 0;
            for (int i = -2; i <= 2; i++) {
                for (int j = -2; j <= 2; j++) {
                    sum += GuassianSmoother[i + 2][j + 2] * m_image[x + i][y + j];
                }
            }
            int magnitude = (qreal)sum * gaussianFactor;
            m_smoothedImage[x][y] = magnitude;
        }
    }

    for (int x = 1; x < m_width - 1; x++) {
        for (int y = 1; y < m_height - 1; y++) {
            //Apply a Sobel operator and store the direction and gradient:
            int sumX = 0;
            int sumY = 0;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    sumX += SobelKernelX[i + 1][j + 1] * m_smoothedImage[x + i][y + j];
                    sumY += SobelKernelY[i + 1][j + 1] * m_smoothedImage[x + i][y + j];
                }
            }
            int magnitude = qSqrt((sumX * sumX) + (sumY * sumY));
            int direction = roundToDirection(atan2(sumY, sumX) * (qreal)180 / (qreal)PI);   //round to 0, 45, 90, or 135 degrees
            m_gradientImage[x][y] = magnitude;
            m_directionImage[x][y] = direction;

        }
    }

    for (int x = 1; x < m_width - 1; x++) {
        for (int y = 1; y < m_height - 1; y++) {
            //Apply non-maximum suppression, using the angle and gradient images
            int direction = m_directionImage[x][y];

            int neighborOneGradient = 0;
            int neighborTwoGradient = 0;
            switch(direction) {
            case 0:
                //on edge if mag > north and south
                neighborOneGradient = m_gradientImage[x][y - 1];
                neighborTwoGradient = m_gradientImage[x][y + 1];
                break;
            case 45:
                //on edge if mag > northwest and southeast
                neighborOneGradient = m_gradientImage[x - 1][y - 1];
                neighborTwoGradient = m_gradientImage[x + 1][y + 1];
                break;
            case 90:
                //on edge if mag > east and west
                neighborOneGradient = m_gradientImage[x + 1][y];
                neighborTwoGradient = m_gradientImage[x - 1][y];
                break;
            case 135:
                //on edge if mag > northeastand southwest
                neighborOneGradient = m_gradientImage[x + 1][y - 1];
                neighborTwoGradient = m_gradientImage[x - 1][y + 1];
            }

            int gradient = m_gradientImage[x][y];
            int deltaOne = gradient - neighborOneGradient;
            int deltaTwo = gradient - neighborTwoGradient;
            if (deltaOne > CANNY_THRESH && deltaTwo > CANNY_THRESH)
                m_finalImage[x][y] = 255;
        }
    }
}


int CannyEdgeFilter::roundToDirection(qreal in) {
    if (in <= 23)
        return 0;
    else if (in <= 68)
        return 45;
    else if (in <= 113)
        return 90;
    else if (in <= 158)
        return 135;
    else if (in <= 203)
        return 0;
    else if (in <= 248)
        return 45;
    else if (in <= 293)
        return 90;
    else
        return 0;
}
