#ifndef SOBELEDGEFILTER_H
#define SOBELEDGEFILTER_H

#include "../util/util.h"

class SobelEdgeFilter
{
public:
    SobelEdgeFilter(ImageMatrix image, int width, int height);

    void run();

    ImageMatrix getImage() { return m_workImage; }


private:
    ImageMatrix m_image;
    ImageMatrix m_workImage;

    int m_width;
    int m_height;
};

#endif // SOBELEDGEFILTER_H
