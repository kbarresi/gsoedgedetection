#include "sobeledgefilter.h"

#include <qmath.h>

SobelEdgeFilter::SobelEdgeFilter(ImageMatrix image, int width, int height)
{
    m_image = image;
    m_width = width;
    m_height = height;

    m_workImage = (ImageMatrix) malloc(sizeof(int*) * width);
    for (int i = 0; i < width; i++)
        m_workImage[i] = (int*) malloc(sizeof(int*) * height);
}

void SobelEdgeFilter::run() {
    for (int x = 1; x < m_width -1; x++) {
        for (int y = 1; y < m_height - 1; y++) {
            //Calculate the x+y kernel:
            int sumX = 0;
            int sumY = 0;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    sumX += SobelKernelX[i + 1][j + 1] * m_image[x + i][y + j];
                    sumY += SobelKernelY[i + 1][j + 1] * m_image[x + i][y + j];
                }
            }
            int magnitude = qSqrt((sumX * sumX) + (sumY * sumY));
            if (magnitude < SOBEL_THRESH)
                m_workImage[x][y] = 0;
            else
                m_workImage[x][y] = magnitude;
        }
    }
}
