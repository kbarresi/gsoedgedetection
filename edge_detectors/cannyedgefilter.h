#ifndef CANNYEDGEFILTER_H
#define CANNYEDGEFILTER_H

#include "../util/util.h"

class CannyEdgeFilter
{
public:
    CannyEdgeFilter(ImageMatrix image, int width, int height);
    ~CannyEdgeFilter();

    void run();

    ImageMatrix getImage() { return m_finalImage; }

private:
    ImageMatrix m_image;

    ImageMatrix m_smoothedImage;
    ImageMatrix m_directionImage;
    ImageMatrix m_gradientImage;
    ImageMatrix m_finalImage;

    int m_width;
    int m_height;


    int roundToDirection(qreal in);

};

#endif // CANNYEDGEFILTER_H
